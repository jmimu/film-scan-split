# Film Scan Splitter

This program is designed to split a film scan into individual negatives.

It is currently limited to:
 - grayscale scans (single channel)
 - films chunks as columns
 - white outside of the films
 - black between the pictures
 - scan can be in any format supported by pillow
 
Example of scan image:
![Scan Example](example.png)

This program automates the process of separating these individual negatives and adjusting brightness, making it easier to manage and edit them individually.

## Requirements
- Python 3.x
- numpy
- matplotlib
- pillow

`pip install numpy matplotlib Pillow`

## How to Use

Run the Python script providing the path to your film scan image as an argument, along with optional parameters:
    ```
    usage: trait1.py [-h] [-c COLS] [-r ROWS] [-b BRIGHTNESS] [-8 TO8BIT] [-m MARGIN] [-o OUT] filename

    positional arguments:
      filename              path of scan file to process

    options:
      -h, --help            show help message and exit
      -c COLS, --cols COLS  number of film columns (default: 4)
      -r ROWS, --rows ROWS  number of negatives on one column (default: 6)
      --brightness, --no-brightness
                            do automatic brightness correction (default: True)
      --to8bit, --no-to8bit
                            do convert to 8 bit, else keep input type (default: True)
      -m MARGIN, --margin MARGIN
                            add margin relative size (default: 0.02)
      -o OUT, --out OUT     output directory (default: out)

    ```

## License
*Film Scan Split* is distributed under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. A copy of this license an be found in the file *LICENSE.txt* included with the source code of this program.
