#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 09:02:25 2024

@author: jmimu
"""
import numpy as np
import os
from PIL import Image
import matplotlib.pyplot as plt
import argparse
import pathlib

Image.MAX_IMAGE_PIXELS = 10000000000

# main params
in_name = '?'
nb_col_max = 4 # max on scan
nb_row_max = 6 # max on scan
out_dir = 'out'

# secondary params
black_value_rel = 0.03 # value between pictures
white_value_rel = 0.98 # value outside film
correct_expo_max = 0.85 # if a column is more than that, not detected as picture
add_margin_rel_neg = 0.02 # relative to negative size
white_cumsum_top_rel = 1e-5 # to find where film starts in column
max_negative_solpe_percentage = 1 # % slope max
show_plots = False
auto_brightness = True
to8bit = True
input_type = None



def parse_params():
    global nb_col_max
    global nb_row_max
    global in_name
    global out_dir
    global auto_brightness
    global to8bit
    global add_margin_rel_neg
       
    parser = argparse.ArgumentParser(description='Split film scan into individual negatives')
    parser.add_argument('filename', # type=argparse.FileType('r'),
                        help='path of scan file to process')
    parser.add_argument('-c', '--cols', type=int, default=nb_col_max,
                        help=f'number of film columns (default: {nb_col_max})')
    parser.add_argument('-r', '--rows', type=int, default=nb_row_max,
                        help=f'number of negatives on one column (default: {nb_row_max})')
    parser.add_argument('--brightness', type=bool, default=auto_brightness, action=argparse.BooleanOptionalAction,
                        help=f'do automatic brightness correction')
    parser.add_argument('--to8bit', type=bool, default=to8bit, action=argparse.BooleanOptionalAction,
                        help=f'do convert to 8 bit, else keep input type')
    parser.add_argument('-m', '--margin', type=float, default=add_margin_rel_neg,
                        help=f'add margin relative size (default: {add_margin_rel_neg})')
    parser.add_argument('-o', '--out', default=out_dir, # type=pathlib.Path,
                        help=f'output directory (default: {out_dir})')

    args = parser.parse_args()
    print(args)
    
    nb_col_max = args.cols
    if nb_col_max<1:
        raise ValueError('cols must be strictly positive')
    nb_row_max = args.rows
    if nb_row_max<1:
        raise ValueError('rows must be strictly positive')
    in_name = args.filename
    out_dir = args.out
    auto_brightness = args.brightness
    to8bit = args.to8bit
    add_margin_rel_neg = args.margin
    
    if len(out_dir)<1:
        out_dir='.'
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)


def merge_close_values(tab, tol):
    if len(tab)<2:
        return tab
    indices_split = np.where(np.diff(tab) > tol)[0] + 1
    groups = np.split(tab, indices_split)
    out = []
    for gr in groups:
        out.append(int(np.mean(gr)))
    return np.array(out)


def image_light_stretch(image, percentile=3., out_max_val=255.):
    buffer_px = min(image.shape)//5
    sub_image = image
    if buffer_px>0:
        sub_image = image[buffer_px:-buffer_px][buffer_px:-buffer_px]
    
    vlow = np.percentile(sub_image, percentile)
    vhi = np.percentile(sub_image, 100-percentile)
    # print(vlow, vhi)
    
    image_out = out_max_val*(image-vlow)/(vhi-vlow) # TODO: add margin?
    image_out = np.clip(image_out, 0., out_max_val)
    return image_out
    

def split_cols(image_path):
    print('split cols on image ', image_path)
    img = Image.open(image_path)
    pixels = np.array(img)
    global input_type
    input_type = pixels.dtype
    pixelsf = np.array(pixels, dtype=np.double)
    
    val_max = pixelsf.max()
    mean_cols = np.mean(pixelsf, axis=0)/val_max
    mean_cols[0] = 1. # make sure to have white on the outside
    mean_cols[-1] = 1.
    
    # get big variations
    vertical_tol_px = 1 + int(pixels.shape[1] / nb_col_max*(max_negative_solpe_percentage/100)) # slope tolerancy
    diffs = mean_cols[:-vertical_tol_px] -  mean_cols[vertical_tol_px:]
    indices_up = np.where(diffs < -(1.-correct_expo_max))[0] + vertical_tol_px//2
    indices_down = np.where(diffs > (1.-correct_expo_max))[0] + vertical_tol_px//2
    
    indices_up = merge_close_values(indices_up, vertical_tol_px)
    indices_down = merge_close_values(indices_down, vertical_tol_px)
    
    if show_plots:
        for idx in indices_up:
            plt.scatter(idx, mean_cols[idx], color='r', marker='o')
        for idx in indices_down:
            plt.scatter(idx, mean_cols[idx], color='b', marker='o')
        
        plt.plot(mean_cols)
        plt.title("By col")
        plt.show()
    
    
    col_w_min = pixels.shape[1] / (nb_col_max+3)
    col_w_max = pixels.shape[1] / (nb_col_max+0)
    
    # find all diffs. Will look for elements below diag wth values close to col_w
    diff_w_matrix = np.subtract.outer(indices_up, indices_down)
    sel = np.where( (diff_w_matrix<col_w_max) & (diff_w_matrix>col_w_min) )
    # have to check that indices are growing in both directions (no overlaping)
    sel = list(zip(list(sel[0]),list(sel[1])))
    
    valid_sel = [True]*len(sel)
    for i in range(1,len(sel)):
        if (sel[i][0]<=sel[i-1][0]) or (sel[i][1]<=sel[i-1][1]):
            #raise ValueError('Error: overlapping cols')
            valid_sel[i-1] = False # TODO: keep biggest interval
    
    cols = []
    for i in range(len(sel)):
        if valid_sel[i]:
            cols.append( (indices_down[sel[i][1]], indices_up[sel[i][0]]) )
    
    print('cols: ',cols)
    if len(cols) > nb_col_max:
        raise ValueError('Error: too many cols')
    
    i = 0
    img_cols = []
    
    for col in cols:
        add_margin_px = int(add_margin_rel_neg*(col[1]-col[0]))
        col0 = max(col[0]-add_margin_px, 0)
        col1 = min(col[1]+add_margin_px, pixels.shape[1]-1)
        img_cols.append(pixels[:,col0:col1])
        out_img = Image.fromarray(img_cols[-1])
        out_img.save(f"{out_dir}/{image_path}_col_{i}.png")
        i = i+1
    return img_cols


# where the black separations should be
def make_separations(top, bottom, nb_neg):
    h = (bottom - top)//nb_neg
    return [top + i* h for i in range(nb_neg+1)]
    

def split_negatives(img_col, num_col, in_name):
    print('split negatives on col ', num_col)
    
    pixels = np.array(img_col)
    # keep only center pixels for stats
    pixelsf = np.array(pixels[:,pixels.shape[1]//4:-pixels.shape[1]//4], dtype=np.double)
    
    val_max = pixelsf.max()
    pixelsf = pixelsf/val_max
    mean_rows = np.mean(pixelsf, axis=1)
    
    # find start
    cumsum_top = (mean_rows-1.).cumsum()/pixels.shape[0]
    top_init = np.where(cumsum_top < -white_cumsum_top_rel)[0][0]
    
    cumsum_bottom = (mean_rows[::-1]-1.).cumsum()/pixels.shape[0]
    bottom_init = pixels.shape[0]-1-np.where(cumsum_bottom < -white_cumsum_top_rel)[0][0]
    
    # set outside to 0: saturated for top and bottom half pic size
    # mean_rows[mean_rows>white_value_rel] = 0.
    neg_h_px = pixels.shape[0] // nb_row_max
    desaturate_size = neg_h_px//2
    part = mean_rows[:desaturate_size]
    part[ part>white_value_rel ] = 0.
    part = mean_rows[-desaturate_size:]
    part[ part>white_value_rel ] = 0.
        
    # search for best combination of separations
    search_area_px = neg_h_px//2
    best_score = 10000.
    best_top = top_init
    best_bottom = bottom_init
    search_step = 2
    for top in range(top_init-search_area_px, top_init+search_area_px, search_step):
        for bottom in range(bottom_init-search_area_px, bottom_init+search_area_px, search_step):
            curr_separ = make_separations(top, bottom, nb_row_max)
            score = sum( [ 0 if (i<0 or i>=pixels.shape[0]) else mean_rows[i] for i in curr_separ] )
            # print(top, bottom, score)
            if score < best_score :
                # print('!')
                best_score = score
                best_top = top
                best_bottom = bottom

    # init_split = make_separations(top_init, bottom_init, nb_row_max)
    best_split = make_separations(best_top, best_bottom, nb_row_max)
    
    # print('init split:',init_split,
    #       '  score: ', sum( [mean_rows[i] for i in init_split] ))
    print('best split:',best_split,'  score: ', best_score)
    
    if show_plots:
        for idx in make_separations(top_init, bottom_init, nb_row_max):
            plt.scatter(idx, 1., color='b', marker='o')
        for idx in best_split:
            plt.scatter(idx, 1., color='r', marker='o')
        plt.plot(mean_rows)
        plt.title("By row")
        plt.show()
        
    rows = []
    for i in range(1,len(best_split)):
        rows.append( (best_split[i-1], best_split[i]) )
    print(rows)
    
    i = 0
    imgs_row = []
    for row in rows:
        add_margin_px = int(add_margin_rel_neg*(row[1]-row[0]))
        row0 = max(row[0]-add_margin_px, 0)
        row1 = min(row[1]+add_margin_px, pixels.shape[0]-1)
        sub_img = img_col[row0:row1,:]
        max_val = 255
        if not to8bit:
            if input_type != np.uint8:
                max_val = 65535
        if auto_brightness:
            sub_img = image_light_stretch(sub_img, out_max_val=max_val)
        if to8bit:
            sub_img = sub_img.astype(np.uint8)
        elif input_type is not None:
            sub_img = sub_img.astype(input_type)
        out_img = Image.fromarray(sub_img)
        imgs_row.append(out_img)
        out_img.save(f"{out_dir}/{in_name}_row_{num_col}_{i}.png")
        i = i+1
    return imgs_row



if __name__ == "__main__":
    parse_params()
    print('Working on ',in_name, ' max size: ', nb_col_max, 'x', nb_row_max)
    print(f'Options: {out_dir=} {auto_brightness=} {to8bit=} {add_margin_rel_neg=}')

    
    all_cols = split_cols(in_name)
    for i, col in enumerate(all_cols):
        all_rows = split_negatives(col, i, in_name)
